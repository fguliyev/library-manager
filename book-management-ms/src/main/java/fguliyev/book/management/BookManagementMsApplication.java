package fguliyev.book.management;

//import fguliyev.common.security.auth.jwt.JwtService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@ComponentScan(basePackages = {"fguliyev.common.security", "fguliyev.user.management"})
//@EntityScan({"fguliyev.common.security.model", "fguliyev.book.management"})
//@EnableJpaRepositories({"fguliyev.common.security", "fguliyev.user.management"})
@SpringBootApplication
public class BookManagementMsApplication {


    public static void main(String[] args) {
        SpringApplication.run(BookManagementMsApplication.class, args);
    }

}
