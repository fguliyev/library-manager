package fguliyev.common.security.util;

import org.springframework.http.HttpStatus;

public class CommonUtils {
    private CommonUtils() {
    }

    public static final String AUTH_HEADER = "Authorization";
    public static final String BEARER = "Bearer";
    public static final String ROLE_CLAIM = "ROLE";

    public   static String removeBearerHeader(String header) {
        if (header.startsWith("Bearer"))
            return header.substring(BEARER.length() + 1);
        else
            throw new RuntimeException("It is not a bearer token");
//            throw new RestErrorResponseException(HttpStatus.BAD_REQUEST,new ErrorResponse("405","It is not Bearer token"));
    }
}
