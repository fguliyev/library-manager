package fguliyev.common.security.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String password;

    private String name;

    private boolean accountNonExpired;

    private boolean accountNonLocked;

    private boolean isCredentialsNonExpired;

    private boolean Enabled;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<UserAuthority> authorities;

    @JoinColumn(name = "token_id")
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private VerificationToken verificationToken;

    @OneToOne
    private UserType userType;
}
