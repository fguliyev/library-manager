package fguliyev.common.security.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity(name = "authorithy")
@NoArgsConstructor
@AllArgsConstructor
public class UserAuthority implements GrantedAuthority {

    @Id
    private String authority;
}
