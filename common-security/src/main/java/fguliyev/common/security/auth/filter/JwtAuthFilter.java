package fguliyev.common.security.auth.filter;

import fguliyev.common.security.auth.jwt.JwtService;
import fguliyev.common.security.util.CommonUtils;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class JwtAuthFilter extends OncePerRequestFilter {

    private final JwtService jwtService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        log.info("Our filter is in action {}", CommonUtils.removeBearerHeader(request.getHeader(CommonUtils.AUTH_HEADER)));
        Claims claims = jwtService.parseToken(CommonUtils.removeBearerHeader(request.getHeader(CommonUtils.AUTH_HEADER)));
        log.info("Parsed claims are {}", claims);
        Authentication authentication = getAuthenticationBearer(claims);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
        log.info("Response is {}", response.getHeader("response"));
    }

    private Authentication getAuthenticationBearer(Claims claims) {
        List<?> roles = claims.get(CommonUtils.ROLE_CLAIM, List.class);
        List<GrantedAuthority> authorityList = roles
                .stream()
                .map(a -> new SimpleGrantedAuthority("ROLE_ " + a.toString()))
                .collect(Collectors.toList());
        return new UsernamePasswordAuthenticationToken(claims.getSubject(), "", authorityList);
    }
}
