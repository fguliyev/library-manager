package fguliyev.user.management.dto;

import fguliyev.user.management.enumeration.UserTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignUpDto {

    private String name;

    private String email;

    private String password;

    private UserTypeEnum userType;
}
