package fguliyev.user.management.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.http.HttpHeaders;

@Data
@AllArgsConstructor
public class AuthResponseDto {

    private HttpHeaders httpHeaders;
    private AccessTokenDto accessTokenDto;
}
