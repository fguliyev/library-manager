package fguliyev.user.management.controller;

import fguliyev.user.management.dto.SignUpDto;
import fguliyev.user.management.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @PostMapping("/sign-in")
    public ResponseEntity<String> authentication() {
        log.info("Sign In method is in action");
        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("response", "This is a demo header");
        return new ResponseEntity<String>(new String("Hello World"), responseHeader, HttpStatus.CREATED);
    }

    @PostMapping
    public ResponseEntity signUp(@RequestBody @Valid SignUpDto signUpDto) {
        log.info("Sign Up method is in action");
        userService.signUp(signUpDto);
        return ResponseEntity.noContent().build();
    }
}
