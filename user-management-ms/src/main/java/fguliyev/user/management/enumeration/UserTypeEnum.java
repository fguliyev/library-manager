package fguliyev.user.management.enumeration;

public enum UserTypeEnum {

    USER(1),
    PUBLISHER(2);

    private final long id;

    UserTypeEnum(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
