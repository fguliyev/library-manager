package fguliyev.user.management;

import fguliyev.common.security.auth.jwt.JwtService;
import fguliyev.common.security.domain.User;
import fguliyev.common.security.domain.UserAuthority;
import fguliyev.user.management.repository.UserRepositroy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.Duration;
import java.util.Set;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
@EntityScan({"fguliyev.common.security.domain", "fguliyev.book.management"})
@EnableJpaRepositories({"fguliyev.common.security", "fguliyev.user.management"})
@ComponentScan(basePackages = {"fguliyev.common.security", "fguliyev.user.management"})
public class UserManagementMsApplication implements CommandLineRunner {

    private final UserRepositroy userRepositroy;
    private final JwtService jwtService;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        SpringApplication.run(UserManagementMsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
