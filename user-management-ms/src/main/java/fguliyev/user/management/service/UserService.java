package fguliyev.user.management.service;

import fguliyev.user.management.dto.SignUpDto;
import fguliyev.user.management.dto.AuthResponseDto;
import fguliyev.user.management.dto.LoginDto;

public interface UserService {

    AuthResponseDto signIn(LoginDto loginDto);

    void signUp(SignUpDto signUpDto);
}
