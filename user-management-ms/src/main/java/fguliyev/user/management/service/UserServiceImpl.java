package fguliyev.user.management.service;

import fguliyev.common.security.domain.User;
import fguliyev.common.security.domain.UserAuthority;
import fguliyev.common.security.domain.UserType;
import fguliyev.common.security.domain.VerificationToken;
import fguliyev.user.management.dto.AuthResponseDto;
import fguliyev.user.management.dto.LoginDto;
import fguliyev.user.management.dto.SignUpDto;
import fguliyev.user.management.repository.UserRepositroy;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private ModelMapper modelMapper;

    private final UserRepositroy userRepositroy;

    public UserServiceImpl(ModelMapper modelMapper, UserRepositroy userRepositroy) {
        this.modelMapper = modelMapper;
        this.userRepositroy = userRepositroy;
    }

    @Override
    public AuthResponseDto signIn(LoginDto loginDto) {
        return null;
    }

    @Override
    public void signUp(SignUpDto signUpDto) {
        userRepositroy.findByUsername(signUpDto.getEmail())
                .ifPresent(u -> System.out.println("Email already exist"));
        User user = modelMapper.map(signUpDto, User.class);
        VerificationToken verificationToken = VerificationToken
                .builder()
                .token(UUID.randomUUID().toString())
                .build();
        user.setVerificationToken(verificationToken);
        Set<UserAuthority> authorities = new HashSet<>();
        authorities.add(new UserAuthority("USER"));
        user.setAuthorities(authorities);
        user.setUserType(new UserType(signUpDto.getUserType().getId()));
        verificationToken.setUser(user);
        userRepositroy.save(user);
    }
}
